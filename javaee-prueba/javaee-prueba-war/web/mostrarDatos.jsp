<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="alumno" scope="request" class="Entidad.InfoAlumno" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
</head>
<body>
<h2>Alumno introdujo los siguientes datos:</h2><br>
Nombre: <jsp:getProperty name="alumno" property="nombre" /> <br>
Primer apellido: <jsp:getProperty name="alumno"
property="primerApellido" /> <br>
Segundo apellido: <jsp:getProperty name="alumno"
property="segundoApellido" /> <br>
</body>
</html>