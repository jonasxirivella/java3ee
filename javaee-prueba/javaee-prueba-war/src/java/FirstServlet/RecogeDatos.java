/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FirstServlet;
import Entidad.InfoAlumno;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

@WebServlet(name = "RecogeDatos", urlPatterns = {"/RecogeDatos"})
public class RecogeDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        InfoAlumno _datosAlumno = new InfoAlumno();
        _datosAlumno.setNombre(request.getParameter("nombre"));
        _datosAlumno.setPrimerApellido(request.getParameter("apellido1"));
        _datosAlumno.setSegundoApellido(request.getParameter("apellido2"));
        request.setAttribute("alumno", _datosAlumno);
        RequestDispatcher rd
                = this.getServletContext().getRequestDispatcher("/mostrarDatos.jsp");
        rd.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}


